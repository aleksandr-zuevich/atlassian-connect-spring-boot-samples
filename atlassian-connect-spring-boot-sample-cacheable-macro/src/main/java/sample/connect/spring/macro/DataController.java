package sample.connect.spring.macro;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
public class DataController {

    @RequestMapping(value = "/data", method = GET, produces = "application/json")
    public AtlassianHostUser getData(@AuthenticationPrincipal AtlassianHostUser hostUser) {
        return hostUser;
    }
}
