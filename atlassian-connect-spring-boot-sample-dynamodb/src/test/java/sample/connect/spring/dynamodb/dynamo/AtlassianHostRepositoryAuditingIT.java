package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphanumeric;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasProperty;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AtlassianHostRepositoryAuditingIT {

    private static final String USER_KEY = "charlie";

    @Autowired
    private AtlassianHostRepository hostRepository;

    @Before
    public void setUp() {
        setJwtAuthenticatedPrincipal(null, USER_KEY);
    }

    private void setJwtAuthenticatedPrincipal(AtlassianHost host, String userKey) {
        AtlassianHostUser hostUser = new AtlassianHostUser(host, Optional.ofNullable(userKey));
        TestingAuthenticationToken authentication = new TestingAuthenticationToken(hostUser, null);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void shouldStoreAuditingFieldsWhenCreatingNewHost() {
        // given:
        final AtlassianHost host = atlassianHost();

        // when:
        hostRepository.save(host);

        // then:
        assertThat(readHostFromDb(host), allOf(
            hasProperty("createdDate", is(notNullValue())),
            hasProperty("createdBy", is(USER_KEY)),
            hasProperty("lastModifiedDate", is(notNullValue())),
            hasProperty("lastModifiedBy", is(USER_KEY))
        ));
    }

    @Test
    public void shouldUpdateLastModifiedWhenUpdatingHost() {
        // given:
        final AtlassianHost host = atlassianHost();
        hostRepository.save(host);
        final Calendar lastModifiedDate = readHostFromDb(host).getLastModifiedDate();

        // when:
        hostRepository.save(host);

        // then:
        assertThat(readHostFromDb(host).getLastModifiedDate(), greaterThan(lastModifiedDate));
    }

    private AtlassianHost atlassianHost() {
        final AtlassianHost host = new AtlassianHost();
        host.setClientKey(randomAlphanumeric(32));
        return host;
    }

    private AtlassianHost readHostFromDb(AtlassianHost host) {
        return hostRepository.findById(host.getClientKey()).orElse(null);
    }
}
