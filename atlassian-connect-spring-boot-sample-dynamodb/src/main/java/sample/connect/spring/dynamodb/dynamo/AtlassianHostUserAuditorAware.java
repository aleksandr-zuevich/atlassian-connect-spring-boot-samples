package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHostUser;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Component
public class AtlassianHostUserAuditorAware implements AuditorAware<String> {

    @NotNull
    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication())
                .map(Authentication::getPrincipal)
                .filter(AtlassianHostUser.class::isInstance)
                .map(AtlassianHostUser.class::cast)
                .flatMap(AtlassianHostUser::getUserKey);
    }
}
