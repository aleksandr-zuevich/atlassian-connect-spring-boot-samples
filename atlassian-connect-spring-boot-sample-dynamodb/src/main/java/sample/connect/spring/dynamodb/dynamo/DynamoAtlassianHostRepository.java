package sample.connect.spring.dynamodb.dynamo;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;
import java.util.Calendar;
import java.util.Optional;

@Component
class DynamoAtlassianHostRepository implements AtlassianHostRepository {

    private final DynamoAtlassianHostCrudRepository dynamoHostRepository;
    private final AtlassianHostUserAuditorAware auditorAware;

    DynamoAtlassianHostRepository(DynamoAtlassianHostCrudRepository dynamoHostRepository,
                                  AtlassianHostUserAuditorAware auditorAware) {
        this.dynamoHostRepository = dynamoHostRepository;
        this.auditorAware = auditorAware;
    }

    @Override
    public Optional<AtlassianHost> findFirstByBaseUrl(String baseUrl) {
        return dynamoHostRepository.findFirstByBaseUrl(baseUrl);
    }

    @Override
    public Optional<AtlassianHost> findFirstByBaseUrlOrderByLastModifiedDateDesc(String baseUrl) {
        return dynamoHostRepository.findFirstByBaseUrlOrderByLastModifiedDateDesc(baseUrl);
    }

    @NotNull
    @Override
    public AtlassianHost save(@NotNull AtlassianHost atlassianHost) {
        setAuditingFields(atlassianHost);
        final DynamoAtlassianHost dynamoAtlassianHost = DynamoAtlassianHost.fromAtlassianHost(atlassianHost);
        return dynamoHostRepository.save(dynamoAtlassianHost);
    }

    private void setAuditingFields(AtlassianHost atlassianHost) {
        // DynamoDB doesn't support auditing like other Spring Data implementations, e.g.:
        // - EnableJpaAuditing
        // - EnableMongoAuditing
        final Calendar now = Calendar.getInstance();
        final String currentAuditor = auditorAware.getCurrentAuditor().orElse(null);
        if (atlassianHost.getCreatedDate() == null) {
            atlassianHost.setCreatedDate(now);
            atlassianHost.setCreatedBy(currentAuditor);
        }
        atlassianHost.setLastModifiedDate(now);
        atlassianHost.setLastModifiedBy(currentAuditor);
    }

    @NotNull
    @Override
    public <S extends AtlassianHost> Iterable<S> saveAll(@NotNull Iterable<S> atlassianHosts) {
        throw new UnsupportedOperationException("save(atlassianHosts)");
    }

    @NotNull
    @Override
    public Optional<AtlassianHost> findById(@NotNull String id) {
        Optional<DynamoAtlassianHost> optional = dynamoHostRepository.findById(id);
        return optional.isPresent() ? Optional.of(optional.get()) : Optional.empty();
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return dynamoHostRepository.existsById(id);
    }

    @NotNull
    @Override
    public Iterable<AtlassianHost> findAll() {
        throw new UnsupportedOperationException("findAll()");
    }

    @NotNull
    @Override
    public Iterable<AtlassianHost> findAllById(@NotNull Iterable<String> ids) {
        throw new UnsupportedOperationException("findAll(ids)");
    }

    @Override
    public long count() {
        return dynamoHostRepository.count();
    }

    @Override
    public void deleteById(@NotNull String id) {
        dynamoHostRepository.deleteById(id);
    }

    @Override
    public void delete(@NotNull AtlassianHost atlassianHost) {
        final DynamoAtlassianHost dynamoAtlassianHost = DynamoAtlassianHost.fromAtlassianHost(atlassianHost);
        dynamoHostRepository.delete(dynamoAtlassianHost);
    }

    @Override
    public void deleteAll(Iterable<? extends AtlassianHost> atlassianHosts) {
        atlassianHosts.forEach(this::delete);
    }

    @Override
    public void deleteAll() {
        dynamoHostRepository.deleteAll();
    }
}
