package sample.connect.spring.basic;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BasicController {

    @IgnoreJwt
    @RequestMapping(value = "/basic", method = RequestMethod.GET)
    public Object getPrincipal(@AuthenticationPrincipal Object principal) {
        return principal;
    }
}
